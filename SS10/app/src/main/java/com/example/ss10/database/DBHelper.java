package com.example.ss10.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

public class DBHelper  extends SQLiteOpenHelper {
    public static final String DB_NAME= "USER";
    public static final int DB_VERSION =1;

    public static String Table_name = "TBL_USER";
    public static String ID ="_id";
    public static  String NAME = "name";
    public  static  String GENDER = "gender";
    public static String Des = "des";

    public DBHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db){

        String sql = "CREATE TABLE "+ Table_name+ "("+
                ID + " INTEGER PRIMARY KEY, "+
                NAME +" TEXT, "+
                GENDER +" TEXT, "+
                Des +" TEXT) ";
        db.execSQL(sql);
    }
    @Override
    public  void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
            String sql = "DROP TABLE IF EXISTS "+ Table_name;
            db.execSQL(sql);
            onCreate(db);

    }

    public String addUser(String user, String gender, String des){
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME,user);
        contentValues.put(GENDER,gender);
        contentValues.put(Des,des);
        long isAdd = db.insert(Table_name,null, contentValues);
        if (isAdd == -1){
            return "Add Fail";
        }
        db.close();
        return "Add success";


    }
    public String updateUser (int id, String user, String gender, String des){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME, user);
        contentValues.put(GENDER,gender);
        contentValues.put(Des,des);
        int isUpdate = db.update(Table_name, contentValues, ID+"=?", new String[] {id+""});
        if (isUpdate > 0){
            return "Update success";
        }
        db.close();
        return "Update Fail";


    }
    public  String deleteUser(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        int isDelete = db.delete(Table_name,ID+"=?", new String[]{id+""});
        if (isDelete > 0){
            return "Update success";
        }
        db.close();
        return "Delete fail";
    }
    public Cursor getAllUser(){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM "+Table_name;
        Cursor c = db.rawQuery(sql,null);
        return c;
    }




}
